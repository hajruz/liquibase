package com.hajruz.liquibase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Основной класс приложения.
 *
 * @author Khaybriev Ruzalin
 * @since 7/13/2021
 */
@SpringBootApplication
public class Application {

    /**
     * Запускает приложение.
     *
     * @param args параметры запуска
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
